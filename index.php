﻿<?php
# send raw http header
header('Content-Type: text/html; charset=UTF-8');

function get_login($id_) {
    return substr(md5($id_), 0, 5);
}


$ability_insert = array();


# если запрос был GET
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    
    
    
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    
    // В суперглобальном массиве $_COOKIE PHP хранит все имена и значения куки текущего запроса.
    // Выдаем сообщение об успешном сохранении.
    if (!empty($_COOKIE['save'])) {
        
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 1);
        setcookie('login', '', 1);
        setcookie('pass', '', 1);
        
        // Выводим сообщение пользователю.
        $messages[] = 'Спасибо, Ваши результаты сохранены - теперь Вы есть в нашей базе данных.<br>';
        
        
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
            
            $messages[] = sprintf('<br>Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.<br>',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass'])
                );
            
        }
        
    }
    
    // Складываем признак ошибок в массив.
    $errors = array();
    
    $errors['fio']  =  !empty($_COOKIE['fio_error']);
    $errors['email']  =  !empty($_COOKIE['email_error']);
    $errors['year']  =  !empty($_COOKIE['year_error']);
    $errors['sex']  =  !empty($_COOKIE['sex_error']);
    $errors['abilities']  =  !empty($_COOKIE['abilities_error']);
    $errors['bio']  =  !empty($_COOKIE['bio_error']);
    $errors['accept']  =  !empty($_COOKIE['accept_error']);
    
    
    // Выдаем сообщения об ошибках.
    if (!empty($errors['fio'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 1);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    if (!empty($errors['email'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 1);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните email.</div>';
    }
    if (!empty($errors['year'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('year_error', '', 1);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните год.</div>';
    }
    if (!empty($errors['sex'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('sex_error', '', 1);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните пол.</div>';
    }
    if (!empty($errors['abilities'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('abilities_error', '', 1);
        // Выводим сообщение.
        $messages[] = '<div class="error">Выберите способность.</div>';
    }
    if (!empty($errors['bio'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('bio_error', '', 1);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните биографию.</div>';
    }
    if (!empty($errors['accept'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('accept_error', '', 1);
        // Выводим сообщение.
        $messages[] = '<div class="error">Поставьте галочку.</div>';
    }
    
    
    
    
    // Складываем предыдущие значения полей в массив, если есть.
    // При этом санитизуем все данные для безопасного отображения в браузере.
    $values = array();
    
    $values['fio'] = empty($_COOKIE['fio_value'])             ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value'])         ? '' : strip_tags($_COOKIE['email_value']);
    $values['year'] = empty($_COOKIE['year_value'])           ? '' : strip_tags($_COOKIE['year_value']);
    $values['sex'] = empty($_COOKIE['sex_value'])             ? '' : strip_tags($_COOKIE['sex_value']);
    $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
    $values['bio'] = empty($_COOKIE['bio_value'])             ? '' : strip_tags($_COOKIE['bio_value']);
    
    
    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    
    if (empty($errors) && !empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
            // TODO: загрузить данные пользователя из БД
            // и заполнить переменную $values,
            printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
            
            
        }
        
        include('form.php');
}
// Иначе, если запрос был методом POST
else {
    
    
    // Проверяем ошибки.
    $errors = FALSE;
    $ability_data = ['god', 'idclip', 'levitation'];
    
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 365 * 24 * 60 * 60);
    }
    // email validate
    if (empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ) {
        setcookie('email_error_incorrect', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('email_value', $_POST['email'], time() + 12 * 30 * 24 * 60 * 60);
    }
    // year validate
    if (empty($_POST['year'])) {
        setcookie('year_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('year_value', $_POST['year'], time() + 12 * 30 * 24 * 60 * 60);
    }
    // sex validate
    $sex_answer = $_POST['sex'];
    if ($sex_answer != '0' and $sex_answer != '1') {
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('sex_value', $_POST['sex'], time() + 12 * 30 * 24 * 60 * 60);
    }
    // abilities validate
    if (empty($_POST['abilities'])) {
        setcookie('abilities_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('abilities_value', json_encode($_POST['abilities']), time() + 365 * 24 * 60 * 60);
    }
    // bio validate
    if (empty($_POST['bio'])) {
        setcookie('bio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('bio_value', $_POST['bio'], time() + 365 * 24 * 60 * 60);
    }
    // accept validate
    if (empty($_POST['accept'])) { # is set?
        setcookie('accept_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('accept_value', $_POST['accept'], time() + 365 * 24 * 60 * 60);
    }
    
    
    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 1);
        setcookie('email_error', '', 1);
        setcookie('year_error', '', 1);
        setcookie('sex_error', '', 1);
        setcookie('bio_error', '', 1);
        setcookie('accept_error', '', 1);
    }
    
    // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
            
            $this_login = $_SESSION['login'];
            
            // Connecting to my DB
            $user = 'u20346';
            $password = '2134325';
            $db = new PDO(
                'mysql:host=localhost;dbname=u20346',
                $user,
                $password,
                array(PDO::ATTR_PERSISTENT => true));
            
            // Trying to UPDATE data
            try {
                $stmt = $db->prepare("UPDATE app5 SET
            fio = ?,
            email = ?,
            year = ?,
            sex = ?,
            ability_god = ?,
            ability_idclip = ?,
            ability_levitation = ?,
            biography = ?
            WHERE login = ?");
                
                $stmt -> execute([
                    $_POST['fio'],
                    $_POST['email'],
                    intval($_POST['year']),
                    $_POST['sex'],
                    $ability_insert['god'],
                    $ability_insert['idclip'],
                    $ability_insert['levitation'],
                    $_POST['bio'],
                    $this_login]);
                
            } catch(PDOException $e) {
                print('Error : ' . $e->getMessage());
                exit();
            }
            
        }
        
        else {
            
            $ability_data = ['god', 'idclip', 'levitation'];
            
            $abilities = $_POST['abilities'];
            
            
            foreach ($ability_data as $ability){
                $ability_insert[$ability] = in_array($ability, $abilities) ? 1 : 0;
            }
            
            // Подключение к моей БД
            $user = 'u20346';
            $password = '2134325';
            
            try {
                $db = new PDO('mysql:host=localhost;dbname=u20346', $user, $password, array(PDO::ATTR_PERSISTENT => true));
            } catch(PDOException $e) {
                exit();
            }
            
            
            try {
                $stmt = $db->prepare("INSERT INTO app5 SET
        login = ?,
        password = ?,
        fio = ?,
        email = ?,
        year = ?,
        sex = ?,
        ability_god = ?,
        ability_idclip = ?,
        ability_levitation = ?,
        biography = ?,
        accept = ? ");
                
                $stmt -> execute([
                    
                    $login,
                    $pass,
                    $_POST['fio'],
                    $_POST['email'],
                    intval($_POST['year']),
                    $_POST['sex'],
                    $ability_insert['god'],
                    $ability_insert['idclip'],
                    $ability_insert['levitation'],
                    $_POST['bio'],
                    $_POST['accept'] ]);
                
            } catch(PDOException $e) {
                print('Error : ' . $e->getMessage());
                exit();
            }
        }
        
        // Сохраняем куку с признаком успешного сохранения.
        setcookie('save', '1');
        
        // Делаем перенаправление.
        header('Location: ./');
}
